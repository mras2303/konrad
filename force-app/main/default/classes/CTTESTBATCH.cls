@isTest
private class CTTESTBATCH {
	static testMethod void CTTESTBATCHBatchProcess(){
		Test.startTest();
        String query = 'SELECT Name FROM Account LIMIT 200';
		CITEST b = new CITEST(query);
		database.executebatch(b,200);
		Test.stopTest();
	}
}
